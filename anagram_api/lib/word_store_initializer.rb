class WordStoreInitializer
  SOURCE_FILE = "./data/dictionary.txt"

  def self.load_data(word_store)
    File.open(SOURCE_FILE, "r").each do |line|
      word = line.strip
      word_store.add(word)
      true
    end
  end
end
