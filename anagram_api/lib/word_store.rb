require 'json'
require_relative 'stats'
class WordStore

  def initialize
    @corpus = Hash.new{|h, k| h[k] = [] }
    @stats = Stats.new
  end

  def add(word)
    existing_words = corpus[master_word(word)]
    unless existing_words.include?(word)
      @stats.add(word)
      existing_words << word
    end
    true
  end

  def anagrams_for(word, limit: nil, skip_proper_nouns: false)
    words = corpus[master_word(word)].dup - [word]
    if skip_proper_nouns
      words = words.select {|word| word[0] == word[0].downcase }
    end
    if limit
      words = words.sample(limit)
    end
    { anagrams: words }
  end

  def delete(word)
    existing_words = corpus[master_word(word)]
    if existing_words.include?(word)
      @stats.delete(word)
      corpus[master_word(word)] = existing_words - [word]
    end
    true
  end

  def delete_all
    @corpus.clear
    @stats = Stats.new
  end

  def stats
    @stats.dup.freeze
  end

  private
  attr_accessor :corpus

  def master_word(word)
    word.chars.sort.join.downcase
  end

  def update_stats(word)
    word_length = word.length
    update_mean_length(word_length)
    update_word_count
    update_minimun_word_length(word_length)
    update_maximum_word_length(word_length)
  end

  def update_word_count
    @stats.word_count += 1
  end

  def update_minimun_word_length(word_length)
    if word_length < @stats.minimum_word_length
      @stats.minimum_word_length = word_length
    end
  end

  def update_maximum_word_length(word_length)
    if word_length > @stats.maximum_word_length
      @stats.maximum_word_length = word_length
    end
  end

  def update_mean_length(word_length)
    total_letters = @stats.mean_word_length * @stats.word_count + word_length
    @stats.mean_word_length = total_letters.to_f / (@stats.word_count + 1)
  end
end
