require 'json'
class Stats
  attr_reader :word_count, :minimum_word_length, :maximum_word_length, :mean_word_length
  def initialize
    @word_length_counts = Hash.new(0)
    @word_count = 0
    @minimum_word_length = Float::INFINITY
    @maximum_word_length = 0
    @mean_word_length = 0
  end

  def add(word)
    word_length = word.length
    @word_length_counts[word_length] += 1
    update_mean_length_on_add(word_length)
    increase_word_count
    update_minimun_word_length(word_length)
    update_maximum_word_length(word_length)
  end

  def delete(word)
    word_length = word.length
    @word_length_counts[word_length] -= 1
    update_mean_length_on_delete(word_length)
    decrease_word_count
    update_minimun_word_length(word_length)
    update_maximum_word_length(word_length)
  end

  def increase_word_count
    @word_count += 1
  end

  def decrease_word_count
    @word_count -= 1
  end

  def update_minimun_word_length(word_length)
    case
    when word_length < @minimum_word_length
      @minimum_word_length = word_length
    when @word_length_counts[word_length] < 1 && word_length == @minimum_word_length
      new_min = word_length + 1
      new_min += 1 while @word_length_counts[new_min] < 1
      @minimum_word_length = new_min
    end
  end

  def update_maximum_word_length(word_length)
    case
    when word_length > @maximum_word_length
      @maximum_word_length = word_length
    when @word_length_counts[word_length] < 1 && word_length == @maximum_word_length
      new_max = word_length - 1
      new_min -= 1 while @word_length_counts[new_max] < 1
      @maximum_word_length = new_max
    end
  end

  def update_mean_length_on_add(word_length)
    total_letters = @mean_word_length * @word_count + word_length
    @mean_word_length = total_letters.to_f / (@word_count + 1)
  end

  def update_mean_length_on_delete(word_length)
    total_letters = @mean_word_length * @word_count - word_length
    @mean_word_length = total_letters.to_f / (@word_count - 1)
  end

  def to_s
    "word count: #{word_count}, minimum word length: #{serializable(minimum_word_length)}, maximum word length: #{maximum_word_length}, mean word length: #{mean_word_length}"
  end

  def to_json
    {word_count: word_count,
     minimum_word_length: serializable(minimum_word_length),
     maximum_word_length: maximum_word_length,
     mean_word_length: mean_word_length}.to_json
  end

  def serializable(value)
    value == Float::INFINITY ? 0 : value
  end
end
