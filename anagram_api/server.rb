require 'sinatra'
require_relative 'lib/word_store'
require_relative 'lib/word_store_initializer'

configure do
  set :app_file, __FILE__
  set port: 3000
  set :environment, :production
end

WORD_STORE = WordStore.new
if ARGV[0] == "preload_data"
  WordStoreInitializer.load_data(WORD_STORE)
end

post '/words.json' do
  body = JSON.parse(request.body.read)
  body["words"].each {|word| WORD_STORE.add(word)}
  [201]
end

delete '/words.json' do
  WORD_STORE.delete_all
  [204]
end

delete '/words/:word.json' do
  WORD_STORE.delete(params[:word])
  [200]
end

get '/anagrams/:word.json' do
  limit = params[:limit]&.to_i
  skip_proper_nouns = params[:skip_proper_nouns] == "true"
  anagrams = WORD_STORE.anagrams_for(params[:word], limit: limit, skip_proper_nouns: skip_proper_nouns)
  [200, anagrams.to_json]
end

get '/stats.json' do
  [200, WORD_STORE.stats.to_json]
end
