require 'minitest/spec'
require 'minitest/autorun'
require_relative '../lib/stats'
require 'json'

# word_count=235886, minimum_word_length=1, maximum_word_length=24, mean_word_length=9.56
describe Stats do
  describe ".initialize" do
    it "sets defaults" do
      stats = Stats.new
      stats.word_count.must_equal 0
      stats.minimum_word_length.must_equal Float::INFINITY
      stats.maximum_word_length.must_equal 0
      stats.mean_word_length.must_equal 0
    end
  end

  describe "#add" do
    it "accepts words, updates stats" do
      stats = Stats.new
      stats.add("bob")
      stats.word_count.must_equal 1
      stats.minimum_word_length.must_equal 3
      stats.maximum_word_length.must_equal 3
      stats.mean_word_length.must_equal 3

      stats.add("taco")
      stats.word_count.must_equal 2
      stats.minimum_word_length.must_equal 3
      stats.maximum_word_length.must_equal 4
      stats.mean_word_length.must_equal 3.5
    end
  end

  describe "#delete" do
    it "decreases stats" do
      stats = Stats.new
      stats.add("bob")
      stats.add("taco")
      stats.word_count.must_equal 2
      stats.minimum_word_length.must_equal 3
      stats.maximum_word_length.must_equal 4
      stats.mean_word_length.must_equal 3.5

      stats.delete("bob")
      stats.word_count.must_equal 1
      stats.minimum_word_length.must_equal 4
      stats.maximum_word_length.must_equal 4
      stats.mean_word_length.must_equal 4

      stats.add("bob")
      stats.delete("taco")
      stats.word_count.must_equal 1
      stats.minimum_word_length.must_equal 3
      stats.maximum_word_length.must_equal 3
      stats.mean_word_length.must_equal 3
    end
  end

  describe "#to_s" do
    it "returns a formatted string" do
      stats = Stats.new
      stats.instance_variable_set(:@word_count, 132)
      stats.instance_variable_set(:@minimum_word_length, 1)
      stats.instance_variable_set(:@maximum_word_length, 15)
      stats.instance_variable_set(:@mean_word_length, 4)

      stats.to_s.must_equal "word count: 132, minimum word length: 1, maximum word length: 15, mean word length: 4"
    end

    it "sets minimum_word_length to 0 if it hasn't been set" do
      stats = Stats.new

      stats.to_s.must_equal "word count: 0, minimum word length: 0, maximum word length: 0, mean word length: 0"
    end
  end

  describe "#to_json" do
    it "returns a json string" do
      stats = Stats.new
      stats.instance_variable_set(:@word_count, 132)
      stats.instance_variable_set(:@minimum_word_length, 1)
      stats.instance_variable_set(:@maximum_word_length, 15)
      stats.instance_variable_set(:@mean_word_length, 4)

      parsed_response = JSON.parse(stats.to_json)

      parsed_response.must_equal({"word_count" => stats.word_count,
                                  "minimum_word_length" => stats.minimum_word_length,
                                  "maximum_word_length" => stats.maximum_word_length,
                                  "mean_word_length" => stats.mean_word_length})
    end

    it "sets minimum_word_length to 0 if it hasn't been set" do
      stats = Stats.new

      parsed_response = JSON.parse(stats.to_json)

      parsed_response["minimum_word_length"].must_equal 0
    end
  end
end

