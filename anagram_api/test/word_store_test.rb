require 'minitest/spec'
require 'minitest/autorun'
require 'mocha/mini_test'
require_relative '../lib/word_store'

describe WordStore do
  describe "#add_word" do
    it "accepts new words, returns true" do
      store = WordStore.new
      response = store.add("bob")
      response.must_equal true
    end

    it "accepts proper nouns" do
      store = WordStore.new
      response = store.add("Bob")
      response.must_equal true
    end

    it "updates the stats" do
      store = WordStore.new

      Stats.any_instance.expects(:add).with("a")

      store.add("a")
    end
  end

  describe "#delete" do
    it "removes word from the word store" do
      store = WordStore.new
      store.add("read")
      store.add("dear")
      response = store.anagrams_for("dare")
      expected_anagrams = ["dear", "read"]
      response[:anagrams].sort.must_equal expected_anagrams
      store.delete("dear")

      response = store.anagrams_for("dare")
      response.must_equal({ anagrams: ["read"] })
    end

    it "updates the stats" do
      store = WordStore.new
      store.add("a")

      Stats.any_instance.expects(:delete).with("a")

      store.delete("a")
    end
  end

  describe "#delete_all" do
    it "removes all of the words in the word store" do
      store = WordStore.new
      store.add("read")
      store.delete_all
      response = store.anagrams_for("dare")
      response.must_equal({ anagrams: [] })
    end

    it "resets all stats" do
      store = WordStore.new
      store.add("read")
      store.delete_all
      stats = store.stats

      stats.word_count.must_equal 0
      stats.minimum_word_length.must_equal Float::INFINITY
      stats.maximum_word_length.must_equal 0
      stats.mean_word_length.must_equal 0
    end
  end

  describe "#anagrams_for" do
    it "returns only anagrams" do
      store = WordStore.new

      store.add("bob")
      # this result is the requested word and shouldn't be in results
      store.add("bbo")
      # this word is not an anagram
      store.add("bib")

      store.anagrams_for("bbo").must_equal({ anagrams: ["bob"] })
    end

    it "skips proper nouns when passed a param for it" do
      store = WordStore.new

      # proper nouns should not be included
      store.add("Bob")
      # this result is the requested word and shouldn't be in results
      store.add("bbo")
      # this word is not an anagram
      store.add("obb")

      store.anagrams_for("bbo", skip_proper_nouns: true).must_equal({ anagrams: ["obb"] })
    end

    it "returns a limited set if a limit is passed" do
      store = WordStore.new

      store.add("read")
      store.add("dare")
      store.add("dear")

      results = store.anagrams_for("read", limit: 1)
      results[:anagrams].size.must_equal 1
    end
  end

  describe "#stats" do
    it "returns the count of words in the store" do
      store = WordStore.new
      response = store.add("bob")
      # add a duplicate to make sure it isn't counted
      response = store.add("bob")
      response = store.add("boo")
      response = store.add("car")
      response = store.add("a")

      stats = store.stats
      stats.word_count.must_equal 4
    end

    it "returns the minimum word length in the store" do
      store = WordStore.new
      response = store.add("bob")
      response = store.add("boo")
      response = store.add("car")
      response = store.add("a")

      stats = store.stats
      stats.minimum_word_length.must_equal 1
    end

    it "returns the maximum word length in the store" do
      store = WordStore.new
      response = store.add("human")
      response = store.add("boo")
      response = store.add("car")
      response = store.add("a")

      stats = store.stats
      stats.maximum_word_length.must_equal 5
    end

    it "returns the mean average word length in the store" do
      store = WordStore.new
      response = store.add("human")
      response = store.add("boo")
      response = store.add("car")
      response = store.add("a")

      stats = store.stats
      stats.mean_word_length.must_equal 3
    end
  end
end

