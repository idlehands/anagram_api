Anagram API
=========

# Usage

---

Using Ruby 2.3.1, make sure to install your gems:

`bundle install`

and then choose how you want to run the server:

- `ruby server.rd` will spin the server up without preloaded data.
- `ruby server.rd preload_data` will spin the server up and preload the data from the dictionary file.

The server meets the spec'd api with two optional features:

- `GET /stats.json` will return the word count, the minimum word length, the maximum word length and the mean average word length
- `GET /anagrams/:word.json?skip_proper_nouns=true` will exclude proper nouns from the results


Running the test suite is done by running a simple rake task:

`rake test`


# Design Decisions

---

I chose to run with a Sinatra API because, given the size of the API, anything else seemes like overkill. That said, I worked to
keep any of the busines logic out of the controller/server file so that, if this project were to grow, the majority of the logic
(the KeyStore and the Stats classes) could easily be ported over into another web framework.

For the KeyStore, I read "you can store as much data in memory as you like" as an invitation to use memory as the data store,
allowing fast read access. The current trade-off is that there is not a way to persist any changes to the corpus, but my read of
the requirements suggested that that was not a priority.

The assignment suggested that the server should ingest the provided dictionary, but the tests provided assumed no data, so I added
an argument that allowed the choice on startup.
